const _COLOR = "#1c3144";
const _BGCOLOR = "rgba(255,255,255,0.1)";
const _MODALBG = "rgba(28, 49, 68, 1)";
const _FontColor = "#000";
const _FontColorLight = "#fff";

// DEFINE T-EYE CLASS
class HifaceInit {

    host = document.location.host;

    userinfo = null


    // STREAMING
    _camera = null;
    get camera() {
        return this._camera;
    }
    set camera(value) {
        this._camera = value;
    }

    constructor() {

        console.log("T-EYE BEGIN");

    }


    // --------------

    update() {

        PATH = document.location.pathname;

        document.title = "HiFACE Access Control Platform";
    }

    _init() {

        // console.log(Date("now").split(" ")[4]);

        // FIX PATH for Login


        if (PATH.indexOf("/app") != -1) {

            // FADE IN BODY                     
            this._afterLoginInit();

        } else {

            this._loginInit();
        }

        // ------ 
    }

    _loginInit() {

        //LOGO
        $(".login").css("background", `url(${_HOST+_IMGURL}/teye-bg.jpg)`)

        // CHANGE LOGO
        $(".logo-image").attr("src", _HOST + _IMGURL + "/teye-logo.png")
        // $(".logo-image").remove()

        $(".login .logo-image").css({

            top: "-20%"
        })

        // LOGIN FORM
        $(".login-right").css({

            position: "absolute",
            left: "12%",
            background: _BGCOLOR,
            color: "white"

        })

        $(".x-input-container .x-input").css({

            "background": "#1c3144",
            "font-size": "14pt",
            "color": "white"
        })
    }

    _afterLoginInit() {

        // COMMON FUNC
        if (this.userinfo == null) {

            // PENDING GET INFO
            this.userinfo = {};

          
            // STREAM
            // this._stream = new Stream();

        }

        // FIX LAYOUT
        this._layoutInit();

        // FILTER
        this._filterToolInit();

        this._tableInit();


        return;

    }

  
    _tableInit() {

        $(".x-box .x-table .x-table-wrapper table thead th").css({
            "color": _FontColorLight,
            "font-weight": "bold"
        })


        $(".app-wrapper").css({
            "background": `url(${_HOST+_IMGURL}/app-bg.jpg) right`,
            "color": "#fff"
        })


        // ATTENDANCE
        $(".com-cal-month .table-day .table-item").css({
            "color": "#fff"
        });

        // TITLE
        $(".com-cal-month .month-title, .week-title").css({
            "background-color": _BGCOLOR,
            "color": "#fff"
        })


        // CHANGE COLOR TABLE
        $(".com-cal-month .table-day .item-bg-gray").css({
            "background-color": _BGCOLOR
        });


    }

    _filterToolInit() {

        // SELECTOR BG
        $(".x-select .x-select-ul").css({

            "background-color": _COLOR
        });

        //CHANGE SELECTED COLOR
        $(".x-select .x-select-ul .x-select-li-true").css({

            "background-color": _COLOR
        })

        // CALENDAR
        $(".com-calendar-month .table-day .table-item").css({
            color: "#fff"
        })

        // INPUT SELECT
        $(".time-input, .x-select .x-select-title").css({

            // background: _COLOR
        })
        // console.log("test");

    }

    _layoutInit() {

        $(".x-box").css("overflow", "hidden");

        // FIX LOGO
        $(".koala-app .koala-sidebar .sidebar-logo").css({

            "max-height": "none",
            "max-width": "none"
        })
        $(".sidebar-logo").attr("src", function (src) {

            if (this.src.indexOf(_HOST) == -1)
                return _HOST + _IMGURL + "/teye-logo.png"
        });

        // REMOVE LANG
        // $(".select-lang").hide();

        // SIDE BAR COLOR
        $(".koala-sidebar").css("background", _COLOR);
        $(".x-box, .app-wrapper-header").css("background", "none")

        $(".app-wrapper-content").css({
            "color": "#fff"
        })

        // CONTENT COLOR
        $('.x-box .x-table-wrapper table tbody').css({
            "color": "#fff"
        })

        // TH
        $(".x-table .x-table-wrapper table thead th,\ .x-table .x-table-wrapper table tbody").css({
            "color": "#fff"
        })

        // VERSION
        $(".version").remove();

        // BUTTON
        $(".x-btn, .export-a-btn").css({

            color: "#fff",
            "background-color": _COLOR
        });
        $(".x-toggle-box").css({
            "background-color": _COLOR
        })

        //    MODAL
        $(".x-modal-content").css({

            "background-color": _MODALBG,
            color: "#fff"
        })

        $("input").css({
            "background": _COLOR,
            color: "#FFF"
        })

        // REMOVE DEVICE WARNING
        let cam_status = `<i class="fa fa-video-camera fa-video-green" style="color: #46ac46;"></i>`
        $(".table-warning").replaceWith(cam_status);

    }


}