const _HOST = "https://cdn.techqila.com";
const _IMGURL = "/assets/images";
const _DOMAIN = "techqila.com";


class Tools {

    _Host = document.location.host;

    constructor() {

    }

    checkDomain(domain) {

        return this._Host.indexOf(domain) != -1 ? true : false;
    }

    checkHttps() {
        console.log("check https");

        let hasNumber = function (myString) {
            return /\d/.test(myString);
        }

        if (location.protocol !== 'https:' && hasNumber(this._Host) == false) {
            location.replace(`https:${location.href.substring(location.protocol.length)}`);
        }
    }

    parseQuery() {

        let urlSearchParams = new URLSearchParams(window.location.search);
        return Object.fromEntries(urlSearchParams.entries());
    }

    request(url, data, type, callback, error_callback) {

        let _parent = this;

        // SUCCESS
        callback = callback || function (data) {

            _parent.Notify(data.code);
        };

        // WARNING
        error_callback = error_callback || (function () {

            // nothing
        })

        type = type || 'POST';

        // NEW DATA -> PUT
        $.ajax({
            url: url,
            type: type,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: callback,
            statusCode: {
                // HANDLE ERRROR
                400: function (data) {

                    // INFORM
                    let res = data.responseJSON;
                    _parent.Notify(res.code, res.message);

                    // CALLBACK
                    error_callback();

                }
            }
        });
    }

    Notify(status, info) {

        info = info || "";

        if (info == "") {

            switch (status) {
                case -1000:
                    info = "Lỗi hệ thống"
                    break;

                case 400:
                    info = "Cảnh báo hệ thống"
                    break;

                default:
                    info = "Cập nhật thông tin thành công"

                    break;
            }
        }

        // -----
        let param = {

            text: info,
            duration: 2000

        };

        if (status == -1000 || status == 400)

            param.style = {
                background: "linear-gradient(to right, #ff5f6d, #ffc371)",
            }

        // ALARM
        Toastify(param).showToast();

        return;
    }

    interval_id = 0;

    hideObject(obj) {

        this.interval_id = setInterval(() => {

            obj.hide();

        }, 100)

        return this.interval_id;
    }

    complete() {

        clearInterval(this.interval_id);

        return;
    }

    second2Array(second) {

        let date = new Date(null);
        date.setSeconds(second); // specify value for SECONDS here
        let result = date.toISOString()

        result = result.substr(11, 5).split(":");

        return {
            hour: parseInt(result[0]),
            minute: parseInt(result[1])
        }
    }

    // chuyển Date sang UnixTime
    DateToUnix(date) {


        if (date == "" || date == null) return "";

        // RE-FORMAT DATE
        let date_obj = new Date(flatpickr.parseDate(date, "Y-m-d H:i"))

        // chuyển thời gian sang hệ unix time
        return flatpickr.formatDate(date_obj, "U");
    }

    reload() {

        setTimeout(() => {

            window.location.reload();

        }, 2000);

        return;

    }

    secondsToHms(d) {

        // console.log(`Late: ${d}`);

        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);

        var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
        return hDisplay + mDisplay + sDisplay;
    }

    triggerInput(enteredName, enteredValue) {
        //         console.log(enteredName, enteredValue);
        const input = document.getElementById(enteredName);

        const lastValue = input.value;
        input.value = enteredValue;
        const event = new Event("input", {
            bubbles: true
        });
        const tracker = input._valueTracker;
        if (tracker) {
            tracker.setValue(lastValue);
        }
        input.dispatchEvent(event);
    }

    loading(item, type) {

        type = type || "dot-spin";

        let load = `<div class="snippet" data-title=".${type}">
        <div class="stage">
          <div class="${type}"></div>
        </div>
      </div>`;

        //   INIT
        item.html(load);

        return;
    }



}

(function ($) {
    $.extend({
        playSound: function () {
            return $(
                '<audio class="sound-player" autoplay="autoplay" style="display:none;">' +
                '<source src="' + arguments[0] + '" />' +
                '<embed src="' + arguments[0] + '" hidden="true" autostart="true" loop="false"/>' +
                '</audio>'
            ).appendTo('body');
        },
        stopSound: function () {
            $(".sound-player").remove();
        }
    });
})(jQuery);